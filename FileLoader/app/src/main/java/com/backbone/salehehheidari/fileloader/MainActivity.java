package com.backbone.salehehheidari.fileloader;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.backbone.salehehheidari.fileloaderlibrary.FileCache;
import com.backbone.salehehheidari.fileloaderlibrary.MultipleItemDownloader;
import com.backbone.salehehheidari.fileloaderlibrary.ItemDownloader;
import com.backbone.salehehheidari.fileloaderlibrary.JsonDownloader;
import com.backbone.salehehheidari.fileloaderlibrary.JsonParser;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    String serverUrl = "http://www.acoderz.com/drupal/appaccess/recentProduct.php";
    String fileServerUrl = "http://www.acoderz.com/drupal/sites/default/files/";
    JsonDownloader jsonDownloader;
    JsonParser jsonParser;
    MultipleItemDownloader multipleItemDownloader;
    String serverJsonResult;
    public static ListView lv_display;
    private static final String actionFilter = "com.backbone.salehehheidari.display";
    FileCache fileCache;

    ArrayList<String> cleanUpArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv_display = (ListView) findViewById(R.id.id_lvdisplay);
        fileCache = new FileCache();

        jsonDownloader = new JsonDownloader(this);

        try {
            serverJsonResult = jsonDownloader.execute(serverUrl).get();
            serverJsonResult = "{\"data\" : " + serverJsonResult + "}";
            jsonParser = new JsonParser(serverJsonResult, "data", "uri");

            cleanUpArray = new ArrayList<String>();

            for(int i = 0; i < jsonParser.GetValueArray().size(); i++) {
                if(jsonParser.GetValueArray().get(i).contains("public://")) {
                    String cleanup = jsonParser.GetValueArray().get(i).replace("public://", "");
                    cleanup = fileServerUrl + cleanup;
                    cleanUpArray.add(cleanup);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        /*

        jsonDownloader = new JsonDownloader(this);
        try {
            serverJsonResult = jsonDownloader.execute(serverUrl).get();
            serverJsonResult = "{\"data\" : " + serverJsonResult + "}";
            jsonParser = new JsonParser(serverJsonResult, "data", "uri");

            cleanUpArray = new ArrayList<String>();

            for(int i = 0; i < jsonParser.GetValueArray().size(); i++) {
                if(jsonParser.GetValueArray().get(i).contains("public://")) {
                    String cleanup = jsonParser.GetValueArray().get(i).replace("public://", "");
                    cleanUpArray.add(cleanup);
                }
            }

            for(String s : cleanUpArray) {
                Log.d("File name", s);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        fileDownloader = new FileDownloader(this, fileServerUrl, actionFilter);
        fileDownloader.execute(cleanUpArray);
        */

        lv_display.setAdapter(new ListViewAdapter(this));
    }

    class ListViewAdapter extends BaseAdapter {
        Context mContext;

        public ListViewAdapter(Context context) {
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return cleanUpArray.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        class Holder {
            ImageView imageView;
        }

        class ItemClickListener implements View.OnClickListener {
            Context mContext;
            int mPosition;
            ItemDownloader mDownloader;
            boolean cancelled;

            public ItemClickListener(Context context, int position, ItemDownloader downloader) {
                this.mContext = context;
                this.mPosition = position;
                this.mDownloader = downloader;
                this.cancelled = false;
            }

            @Override
            public void onClick(View v) {
                cancelled = !cancelled;

                if(cancelled) {
                    this.mDownloader.cancel(true);
                } else {
                    ItemDownloader fileDownloaderSeparate = new ItemDownloader(this.mContext); //if the bitmap does not exist yet, then download that bitmap and apply it to the imageview
                    fileDownloaderSeparate.SetUrl(cleanUpArray.get(this.mPosition));
                    fileDownloaderSeparate.SetTargetView(v);

                    fileDownloaderSeparate.SetLoadingImage(this.mContext.getResources().getDrawable(R.drawable.spinner));
                    fileDownloaderSeparate.SetCancelledImage(this.mContext.getResources().getDrawable(R.drawable.cancelled));
                    fileDownloaderSeparate.execute();
                }

                Toast.makeText(this.mContext, "Cancelled = " + String.valueOf(cancelled), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = new Holder();

            holder.imageView = new ImageView(this.mContext);
            ItemDownloader fileDownloaderSeparate = new ItemDownloader(this.mContext); //if the bitmap does not exist yet, then download that bitmap and apply it to the imageview
            fileDownloaderSeparate.SetUrl(cleanUpArray.get(position));
            fileDownloaderSeparate.SetTargetView(holder.imageView);

            fileDownloaderSeparate.SetLoadingImage(this.mContext.getResources().getDrawable(R.drawable.spinner));
            fileDownloaderSeparate.SetCancelledImage(this.mContext.getResources().getDrawable(R.drawable.cancelled));


            if(FileCache.bitmapLruCache.get(cleanUpArray.get(position)) != null) {
                holder.imageView.setImageBitmap(FileCache.bitmapLruCache.get(cleanUpArray.get(position))); //if the bitmap already exists, then use that bitmap
            } else {
                fileDownloaderSeparate.execute();
            }

            holder.imageView.setOnClickListener(new ItemClickListener(this.mContext, position, fileDownloaderSeparate));

            return holder.imageView;
        }
    }
}
