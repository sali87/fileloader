package com.backbone.salehehheidari.fileloader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class DisplayReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        MainActivity.lv_display.invalidateViews();
    }
}
