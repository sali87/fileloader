package com.backbone.salehehheidari.fileloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.backbone.salehehheidari.fileloaderlibrary.MultipleItemDownloader;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class ListViewAdapter extends BaseAdapter {
    Context mContext;
    MultipleItemDownloader mMultipleItemDownloader;

    public ListViewAdapter(Context context, MultipleItemDownloader multipleItemDownloader) {
        this.mContext = context;
        this.mMultipleItemDownloader = multipleItemDownloader;
    }

    @Override
    public int getCount() {
        return mMultipleItemDownloader.GetKeys().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(this.mContext);

        String key = mMultipleItemDownloader.GetKeys().get(position);
        Bitmap bitmap = mMultipleItemDownloader.GetBitmapCache().get(key);

        imageView.setImageBitmap(bitmap);

        return imageView;
    }
}
