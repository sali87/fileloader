package com.backbone.salehehheidari.fileloaderlibrary;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class MultipleItemDownloader extends AsyncTask<ArrayList<String>, Void, Void> {
    Context mContext;
    LruCache<String, Bitmap> bitmapLruCache;
    LruCache<String, String> jsonLruCache;
    LruCache<String, String> xmlLruCache;
    ArrayList<String> fileKeys;
    int cacheSize;
    ProgressDialog progressDialog;
    String mServerUrl;
    String mActionFilter;

    public MultipleItemDownloader(Context context, String serverUrl, String actionFilter) {
        this.mContext = context;
        this.cacheSize = 10 * 1024 * 1024;
        this.bitmapLruCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return super.sizeOf(key, value);
            }
        };
        this.fileKeys = new ArrayList<String>();
        this.mServerUrl = serverUrl;
        this.mActionFilter = actionFilter;
    }

    public LruCache<String, Bitmap> GetBitmapCache() {
        return this.bitmapLruCache;
    }

    public ArrayList<String> GetKeys() {
        return this.fileKeys;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(this.mContext, "Loading", "", true);
    }

    @Override
    protected Void doInBackground(ArrayList<String>... params) {
        ArrayList<String> fileStrings = params[0];

        for(String s : fileStrings) {
            int ResponseCode = -1;
            try {
                URL url = new URL(this.mServerUrl + s);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setDoInput(true);
                urlConnection.connect();
                ResponseCode = urlConnection.getResponseCode();
                InputStream inputStream;
                OutputStream outputStream;
                File outputFile;

                if(ResponseCode == HttpURLConnection.HTTP_OK) {
                    inputStream = new BufferedInputStream(url.openStream(), 8192);
                    outputFile = new File(this.mContext.getCacheDir().toString() + File.separator + s);

                    if(outputFile.exists()) {
                        outputFile.delete();
                    }

                    outputStream = new FileOutputStream(outputFile);

                    byte[] data = new byte[1024];

                    while(inputStream.read(data) != -1) {
                        outputStream.write(data);
                    }

                    Bitmap tempBitmap = BitmapFactory.decodeFile(outputFile.toString());

                    bitmapLruCache.put(s, tempBitmap);
                    fileKeys.add(s);

                    Log.d("file path", outputFile.toString());

                    outputStream.flush();

                    outputStream.close();

                    inputStream.close();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();

        Intent displayIntent = new Intent();
        displayIntent.setAction(this.mActionFilter);
        this.mContext.sendBroadcast(displayIntent);
    }
}
