package com.backbone.salehehheidari.fileloaderlibrary;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class JsonDownloader extends AsyncTask<String, Void, String>{
    Context mContext;
    ProgressDialog progressDialog;
    BufferedReader reader;
    InputStream inputStream;
    int ResponseCode = -1;
    HttpURLConnection urlConnection;

    public JsonDownloader(Context context) {
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(this.mContext, "Loading", "Please Wait...", true);
    }

    @Override
    protected String doInBackground(String... params) {
        String lineData = "";
        String jsonString = "";

        try {
            URL url = new URL(params[0]);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.connect();

            inputStream = urlConnection.getInputStream();
            ResponseCode = urlConnection.getResponseCode();

            if(ResponseCode == HttpURLConnection.HTTP_OK){
                inputStream = urlConnection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

                while((lineData = reader.readLine()) != null) {
                    jsonString += lineData;
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressDialog.dismiss();
    }
}
