package com.backbone.salehehheidari.fileloaderlibrary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class ItemDownloader extends AsyncTask<Void, Void, Bitmap> {

    Context mContext;
    String mFileUrl;
    View mTargetView;
    Drawable mLoadingImage;
    Drawable mCancelImage;
    URL url;
    HttpURLConnection urlConnection;
    InputStream inputStream;
    Boolean cancelled;


    public ItemDownloader(Context context) {
        this.mContext = context;
        cancelled = false;
    }

    public void SetUrl(String fileUrl) {
        this.mFileUrl = fileUrl;
    }

    public void SetTargetView(View targetView) {
        this.mTargetView = targetView;
    }

    public void SetLoadingImage(Drawable loadingImage) {
        this.mLoadingImage = loadingImage;
    }

    public void SetCancelledImage(Drawable cancelImage) {
        this.mCancelImage = cancelImage;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(this.mLoadingImage != null) {
            ((ImageView)this.mTargetView).setImageDrawable(this.mLoadingImage);
        }
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap loadedBitmap = null;
        if(this.mFileUrl != null) {
            try {
                url = new URL(this.mFileUrl);
                urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setDoInput(true);
                urlConnection.connect();

                inputStream = urlConnection.getInputStream();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);

                int originalWidth = options.outWidth;
                int originalHeight = options.outHeight;

                int scaleFactor = 1;

                int targetWidth = 160;
                int targetHeight = 120;

                scaleFactor = Math.min(originalWidth/targetWidth, originalHeight/targetHeight);
                options.inJustDecodeBounds = false;
                options.inSampleSize = scaleFactor;

                inputStream.close();

                urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setDoInput(true);
                urlConnection.connect();
                inputStream = urlConnection.getInputStream();

                loadedBitmap = BitmapFactory.decodeStream(inputStream, null, options);

                inputStream.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return loadedBitmap;
    }

    @Override
    protected void onPostExecute(Bitmap mBitmap) {
        super.onPostExecute(mBitmap);

        if(mBitmap != null) {
            FileCache.bitmapLruCache.put(this.mFileUrl, mBitmap);
        } else {
            Bitmap tempBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.cancelled);
            FileCache.bitmapLruCache.put(this.mFileUrl, tempBitmap);
        }

        ((ImageView) this.mTargetView).setImageBitmap(FileCache.bitmapLruCache.get(this.mFileUrl));
        this.mTargetView.invalidate();

        Log.d("Bitmap Size", String.valueOf(FileCache.bitmapLruCache.get(this.mFileUrl).getByteCount()));
    }
}
