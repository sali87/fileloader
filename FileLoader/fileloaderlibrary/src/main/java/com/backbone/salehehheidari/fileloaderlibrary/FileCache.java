package com.backbone.salehehheidari.fileloaderlibrary;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class FileCache {
    public static final int cacheSize = 10 * 1024 * 1024;
    public static LruCache<String, Bitmap> bitmapLruCache;
    public static LruCache<String, String> jsonLruCache;
    public static LruCache<String, String> xmlLruCache;


    public FileCache() {
        bitmapLruCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount() / 1024;
            }
        };

        jsonLruCache = new LruCache<String, String>(cacheSize) {
            @Override
            protected int sizeOf(String key, String value) {
                return super.sizeOf(key, value);
            }
        };

        xmlLruCache = new LruCache<String, String>(cacheSize) {
            @Override
            protected int sizeOf(String key, String value) {
                return super.sizeOf(key, value);
            }
        };
    }
}
