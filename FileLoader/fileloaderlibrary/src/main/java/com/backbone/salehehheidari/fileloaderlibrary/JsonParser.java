package com.backbone.salehehheidari.fileloaderlibrary;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by BackboneEntertainment on 10/05/16.
 */
public class JsonParser {
    JSONObject jsonObject;
    JSONArray jsonArray;
    ArrayList<String> valueArray;

    public JsonParser(String inputString, String rootKey, String itemKey) {
        valueArray = new ArrayList<String>();
        try {
            jsonObject = new JSONObject(inputString);
            jsonArray = jsonObject.optJSONArray(rootKey);
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                String jsonValue = jsonItem.getString(itemKey);
                valueArray.add(jsonValue);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> GetValueArray() {
        return this.valueArray;
    }
}
